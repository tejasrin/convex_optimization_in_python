import numpy as np
import scipy 
import matplotlib.pyplot as plt

c = float(input("Enter your value: "))#Input value of c is entered here
print(c)
def sq(x,c):
	return x*x*x - 3*x*c

#Plotting the parabola
x = np.linspace(-5,5,50)#points on the x axis
vec_sq = scipy.vectorize(sq)
f=vec_sq(x,c)#Objective function
plt.plot(x,f,color=(1,0,1))
plt.grid()
plt.xlabel('$x$')
plt.ylabel('$x*x*x-3*x*c $')

#Convexity/Concavity
a = 1
b = 4
lamda = 0.3
t = lamda *a + (1-lamda)*b
f_a = sq(a,c)
f_b = sq(b,c)

f_t = sq(t,c)
f_t_hat = lamda *f_a + (1-lamda)*f_b

#Plot commands
plt.plot([a,a],[0,f_a],color=(1,0,0),marker='o',label="$f(a)$")
plt.plot([b,b],[0,f_b],color=(0,1,0),marker='o',label="$f(b)$")
plt.plot([t,t],[0,f_t],color=(0,0,1),marker='o',label="$f(\lambda a + (1-\lambda)b)$")
plt.plot([t,t],[0,f_t_hat],color=(1/2,2/3,3/4),marker='o',label="$\lambda f(a) + (1-\lambda)f(b)$")
plt.plot([a,b],[f_a,f_b],color=(0,1,1))
plt.legend(loc=2)
plt.savefig('../figs/1.2_0.pdf')
#subprocess.run(shlex.split("termux-open ../figs/1.2.pdf"))
plt.show()#Reveals the plot








